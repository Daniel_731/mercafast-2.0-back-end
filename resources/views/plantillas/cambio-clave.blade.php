<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8"/>
   <meta name="viewport"
         content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
   <meta http-equiv="X-UA-Compatible" content="ie=edge"/>

   <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.10.2/css/all.css"
   />


   <style type="text/css">

      @import url('https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap');


      /* General */
      * {
         font-family: 'Montserrat', sans-serif !important;
      }

      .m-3 {
         margin: 1rem !important;
      }

      img {
         height: 20px;
         border: 1px solid #FFFFFF;
         border-radius: 10px;
      }

      img.facebook {
         padding: 10px 13.76px;
      }

      img.twitter {
         padding: 10px 11.25px;
      }

      img.instagram {
         padding: 10px 11.25px;
      }

      img.linkedin {
         padding: 10px;
      }


      #container {
         max-width: 1140px;
         margin: 0 auto;
      }


      header {
         min-height: 100px;
         max-height: 100px;
         background-color: #000000 !important;
      }

      header > h1 {
         color: #FFFFFF;
         font-size: 40px;
         font-weight: 700;
         margin: 0;
         text-align: center;
         padding-top: 24px;
      }


      article {
         padding: 10px 20px;
         text-align: justify;
      }


      footer {
         min-height: 100px;
         max-height: 100px;
         background-color: #000000 !important;
      }

      #container_buttons_social_media {
         text-align: center;
         padding-top: 30px;
      }

      footer a {
         color: transparent;
      }


      blockquote {
         text-align: center;
         font-size: 13px;
         opacity: 0.5;
      }


      .btn {
         display: inline-block;
         font-weight: 400;
         color: #212529;
         text-align: center;
         vertical-align: middle;
         -webkit-user-select: none;
         -moz-user-select: none;
         -ms-user-select: none;
         user-select: none;
         background-color: #0000;
         border: 1px solid #0000;
         padding: .375rem .75rem;
         font-size: 1rem;
         line-height: 1.5;
         border-radius: .25rem;
         transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
      }

      .btn-primary {
         color: #fff;
         background-color: #2196f3;
         border-color: #2196f3;
      }

      a.btn {
         color: #FFFFFF !important;
         text-decoration: none !important;
      }

   </style>
</head>


<body>

<div id="container">
   <header>
      <h1>Mercafast</h1>
   </header>

   <article>
      <h2> {{ $name }},</h2>

      <div style="padding-left: 30px;">
         <p>Estamos muy complacidos de que se haya unido a Mercafast.</p>

         <p>Recuerdad debes Ingresar por el mismo medio que te Registraste</p>
         <h4>Estas son tus credenciales de Ingreso</h4>

         <p>
            <strong><b>Correo Electronico : </b> {{ $email}}</strong> .
         </p>
         <p>
            <strong><b>Contraseña : </b> {{ $clave }}</strong> .
         </p>

         <p>Entre en su perfil de usuario, copie su enlace de referido, refiera a sus amigos Odontólogos para que gane más con REFEREX de RADOMAX.</p>

         <p><a href="http://radomax-fronted.radomax.link/#/" target="_blank">Mercafast</a></p>
      </div>
   </article>

   <footer>
      <div id="container_buttons_social_media">
         <a href="https://www.facebook.com/Golondrix" target="_blank" class="btn btn-outline-info rounded-circle">
            <img src="https://i.ibb.co/6Wffv2W/facebook-f-brands.png" class="facebook"/>
         </a>

         <span class="m-3"></span>

         <a href="https://twitter.com/golondrix" target="_blank" class="btn btn-outline-info rounded-circle">
            <img src="https://i.ibb.co/9ZgwGXF/instagram-brands.png" class="twitter"/>
         </a>

         <span class="m-3"></span>

         <a href="https://www.instagram.com/Golondrix" target="_blank" class="btn btn-outline-info rounded-circle">
            <img src="https://i.ibb.co/q5qcJ18/linkedin-in-brands.png" class="instagram"/>
         </a>

         <span class="m-3"></span>

         <a href="https://www.linkedin.com/company/golondrix" target="_blank"
            class="btn btn-outline-info rounded-circle">
            <img src="https://i.ibb.co/8jmnmqk/twitter-brands.png" class="linkedin"/>
         </a>
      </div>
   </footer>

   <blockquote>© Mercafast 2020. Powered by DEGM. Derechos Reservados</blockquote>
</div>

</body>

</html>
