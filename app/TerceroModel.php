<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $establecimiento_codigo
 * @property integer $usuario_codigo
 * @property string $name
 * @property string $documento
 * @property string $barrio
 * @property string $direccion
 * @property integer $telefono
 * @property string $foto
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property TbEstablecimiento $tbEstablecimiento
 * @property TbNota[] $tbNotas
 */
class TerceroModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tb_tercero';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['establecimiento_codigo', 'usuario_codigo', 'name', 'documento', 'barrio', 'direccion', 'telefono', 'foto', 'created_at', 'updated_at','estado'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'usuario_codigo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tbEstablecimiento()
    {
        return $this->belongsTo('App\TbEstablecimiento', 'establecimiento_codigo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tbNotas()
    {
        return $this->hasMany('App\TbNota', 'tercero_codigo');
    }
}
