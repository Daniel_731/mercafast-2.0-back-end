<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $nota_codigo
 * @property string $pedido
 * @property string $created_at
 * @property string $updated_at
 * @property TbNota $tbNota
 */
class DetalleNotasModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tb_itemnotas';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['nota_codigo', 'pedido', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tbNota()
    {
        return $this->belongsTo('App\TbNota', 'nota_codigo');
    }
}
