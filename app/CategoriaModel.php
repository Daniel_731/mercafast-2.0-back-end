<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property string $imagen
 * @property TbEstablecimiento[] $tbEstablecimientos
 * @property TbTipoproducto[] $tbTipoproductos
 */

class CategoriaModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tb_categoria';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'description', 'created_at', 'updated_at', 'imagen','url'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tbEstablecimientos()
    {
        return $this->hasMany('App\TbEstablecimiento', 'catecoria_codigo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tbTipoproductos()
    {
        return $this->hasMany('App\TbTipoproducto', 'categoria_codigo');
    }
}
