<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer $id
 * @property integer $codigo_onesignal
 * @property string $token_onesignal
 * @property User $usuario_codigo
 */
class UsuarioDispositivoModel extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'tb_usuariodispositivo';

	/**
	 * The "type" of the auto-incrementing ID.
	 *
	 * @var string
	 */
	protected $keyType = 'integer';

	/**
	 * @var array
	 */
    protected $fillable = [
        'usuario_codigo', 
        'codigo_onesignal',
        'token_onesignal',
        'created_at',
        'updated_at'];

	/**
	 * @return BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo('App\User', 'usuario_codigo');
	}

	/**
	 * @return BelongsTo
	 */

}
