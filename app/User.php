<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $barrio
 * @property string $direccion
 * @property string $email_verified_at
 * @property string $password
 * @property string $created_at
 * @property string $updated_at
 * @property string $estado
 * @property integer $tipo_usuario
 * @property string $ciudad
 * @property string $ubicacion
 * @property boolean $membresia
 * @property string $imagen
 * @property string $url_imagen
 * @property int $tipo_login
 * @property FavoritoModel[] $tbFavoritos
 */
class User extends Authenticatable
{
	use HasApiTokens, Notifiable;

	/**
	 * The "type" of the auto-incrementing ID.
	 *
	 * @var string
	 */
	protected $keyType = 'integer';

	/**
	 * @var array
	 */
	protected $fillable = ['name',
		'email',
		'barrio',
		'direccion',
		'email_verified_at',
		'password',
		'created_at',
		'updated_at',
		'estado',
		'tipo_usuario',
		'ciudad',
		'ubicacion',
		'membresia',
		'imagen',
		'url_imagen',
		'tipo_login',
		'device_name',
      'tipo_usuario_front'
	];

	/**
	 * @return HasMany
	 */
	public function tb_favoritos()
	{
		return $this->hasMany('App\FavoritoModel', 'usuario_codigo');
	}

	public function tb_usuariodispositivo()
	{
		return $this->belongsTo('App\UsuarioDispositivoModel', 'usuario_codigo');
	}
}
