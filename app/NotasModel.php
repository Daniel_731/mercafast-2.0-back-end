<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $negocio_codigo
 * @property string $titulo
 * @property string $descripcion
 * @property string $domicilio
 * @property float $valor
 * @property float $valor_domicilio
 * @property integer $usuario_codigo
 * @property integer $tercero_codigo
 * @property string $created_at
 * @property string $updated_at
 * @property string $latitud
 * @property string $longitud
 * @property TbEstablecimiento $tbEstablecimiento
 * @property TbItemnota[] $tbItemnotas
 */
class NotasModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tb_notas';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['negocio_codigo',
    'titulo',
    'descripcion',
    'domicilio',
    'valor',
    'valor_domicilio',
    'usuario_codigo',
    'tercero_codigo',
    'created_at',
    'updated_at',
    'latitud',
    'longitud',
    'estado',
    'tiempo_estimado'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tbEstablecimiento()
    {
        return $this->belongsTo('App\TbEstablecimiento', 'negocio_codigo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tbItemnotas()
    {
        return $this->hasMany('App\TbItemnota', 'nota_codigo');
    }
}
