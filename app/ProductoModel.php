<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $establecimeinto_codigo
 * @property integer $tipo_producto
 * @property string $titulo
 * @property string $descripcion
 * @property string $domicilio
 * @property float $valor
 * @property float $descuento
 * @property string $created_at
 * @property string $updated_at
 * @property TbEstablecimiento $tbEstablecimiento
 * @property TbTipoproducto $tbTipoproducto
 */
class ProductoModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tb_productos';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['establecimeinto_codigo', 'tipo_producto', 'titulo', 'descripcion', 'domicilio', 'valor', 'descuento','imagen','url', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tbEstablecimiento()
    {
        return $this->belongsTo('App\TbEstablecimiento', 'establecimeinto_codigo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tbTipoproducto()
    {
        return $this->belongsTo('App\TbTipoproducto', 'tipo_producto');
    }
}
