<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FavoritoModel;
use Illuminate\Support\Facades\DB;



class FavoritosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $id = 35;
        $favoritos = DB::table('tb_establecimiento')
                    ->join('tb_favorito','tb_establecimiento.id','=', 'tb_favorito.negocio_codigo')
                    ->join('users','tb_favorito.usuario_codigo','=', 'users.id')
                    ->select('tb_establecimiento.name as name_negocio', 
                            'tb_establecimiento.direccion as name_direccion',
                            'tb_establecimiento.foto as foto_negocio', 
                            'users.name as name_user',
                            'tb_establecimiento.id as id_negocio',
                            'users.id as id_usuario')
                    ->where('tb_favorito.usuario_codigo','=', $id)->get();
                   
                    return response()->json([
                        "favoritos" => $favoritos
                    ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $borrarTexto = 'Se removio de mis Favoritos';
        $deleteFavorito = FavoritoModel::query()->where('usuario_codigo', $request->usuario)
                                ->where('negocio_codigo',$request->negocio)->first();
        

      $boroo =   $deleteFavorito->delete(); 

        if( $boroo) {
            return response()->json([
                'respuesta'=> $borrarTexto  
              ]);
        }else {}
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
