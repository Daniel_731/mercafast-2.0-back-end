<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EstablecimientoModel;
use Storage;
use DateTime;

class EstablecimientosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $establecimiento = EstablecimientoModel::query()->select(
            'id','name','descripcion','barrio','direccion','foto')
            ->orderBy('id','DESC')->get();
            
            return response()->json([
                'establecimiento' => $establecimiento
             ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        // return response()->json([
        //     "respuesta" => $request->all()
        // ]);

    $imagen64 = $request->all()['foto'];

     $domiciliario = "";

     $url = null;

     if ($imagen64) {

        $replace = substr($imagen64, 0, strpos($imagen64, ',')+1);
        $image = str_replace($replace, '', $imagen64);
        $image = str_replace(' ', '+', $image);

        $nombre = new DateTime();
        $nombre = $nombre->getTimestamp().".png";
        Storage::disk('negocios')->put($nombre, base64_decode($image));
        
        $url = $request->root().'/storage/negocios/'.$nombre;
        
        $request['foto'] = $url;
     }

        $domiciliario = "";
        $respuesta =  EstablecimientoModel::create($request->all());
   
        if( $respuesta) {
            $domiciliario =    "Registro creado";
        } else {
           $domiciliario = "Error al grabar";
        }
   
           return response()->json([
               "respuesta" => $domiciliario
           ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $establecimeinto = EstablecimientoModel::where('id', '=', $id)->get();

        return response()->json([
            "establecimeinto" => $establecimeinto
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      //  return response()->json($id);
        if ($id == null || $id == "null" || $id == "") {
            
            $establecimiento = EstablecimientoModel::query()->select(
            'id','name','descripcion','barrio','direccion','foto')
            ->orderBy('id','DESC')->get();
            // return response()->json('--------> sin id');

        }  else {

            $establecimiento = EstablecimientoModel::query()->select(
                'id','name','descripcion','barrio','direccion','foto')
                ->where('catecoria_codigo', $id)
                ->orderBy('id','DESC')->get();
                // return response()->json('--------> con id');
        }
       
        return response()->json([
            "respuesta" => $establecimiento
        ]);
        //return response()->json( $establecimiento);
    }

    public function establecimientoUnicoPrincipal($id) {

        $establecimiento = EstablecimientoModel::query()
            ->where('id', $id)
            ->orderBy('id','DESC')->get();
         
            return response()->json([
                "respuesta" => $establecimiento
            ]);

        //return response()->json($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
     $update  =  EstablecimientoModel::find($id)->update($request->all());

     $respuesta = "";
     if($update) {
        $respuesta = "Registro actualizado";
     }else {
        $respuesta = "No se pudo Actualizar";
     }
        return response()->json([
            "respuesta" => $respuesta
        ]);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
