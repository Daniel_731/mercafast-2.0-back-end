<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TerceroModel;
use Storage;
use DateTime;

class TerceroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $domiciliario = TerceroModel::query()->select(
            'id','name', 'documento', 'direccion','foto','telefono')
            ->orderBy('id','DESC')->get();
            
            return response()->json([
                'domiciliario' => $domiciliario
             ]);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // return response()->json([
        //     "respuesta" =>  $request->root()
        // ]);


    $imagen64 = $request->all()['foto'];

     $domiciliario = "";

     $url = null;

     if ($imagen64) {

        $replace = substr($imagen64, 0, strpos($imagen64, ',')+1);
        $image = str_replace($replace, '', $imagen64);
        $image = str_replace(' ', '+', $image);

        $nombre = new DateTime();
        $nombre = $nombre->getTimestamp().".png";
        Storage::disk('terceros')->put($nombre, base64_decode($image));
        
        $url = $request->root().'/storage/terceros/'.$nombre;
        
        $request['foto'] = $url;
     }


     $respuesta =    TerceroModel::create($request->all());

     if( $respuesta) {
         $domiciliario =    "Tercero creado Exitosamente";
     } else {
        $domiciliario = "Error al grabar";
     }

        return response()->json([
            "respuesta" => $domiciliario
        ]);

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $tercero = TerceroModel::where('id', '=', $id)->get();

        return response()->json([
            "tercero" => $tercero
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tercero = TerceroModel::where('usuario_codigo', '=', $id)->get();
        return response()->json([
            "tercero" => $tercero
        ]);  
    }

    public function TerceroConfirmado($id)
    {
        $tercero = TerceroModel::where('id', '=', $id)->get();
        return response()->json([
            "tercero" => $tercero
        ]);  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $update  =  TerceroModel::find($id)->update($request->all());

        $respuesta = "";
        if($update) {
           $respuesta = "Tercero actualizado Exitosamente ";
        }else {
           $respuesta = "No se pudo Actualizar";
        }
           return response()->json([
               "respuesta" => $respuesta
           ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
