<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\DetalleNotasModel;
use App\NotasModel;
use Illuminate\Support\Facades\DB;
use App\User;
use App\EstablecimientoModel;


class NotasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
    $condicion = "";
        
        /**
         * Para ahorar codigo cuando llega el id consulto en la tabla usuario
         * Que tipo d eusuario es si es cliente traigo todos los pedidos del cliente
         * si es vendedor traigo igual todos los que didos que se le han solicitado
         */
        $tipoUsuario = User::query()->where('id', $id)->first();

        if($tipoUsuario['tipo_usuario'] == 1) {
            $notas = DB::table('tb_notas')
            ->join('tb_establecimiento','tb_notas.negocio_codigo','=','tb_establecimiento.id')
            ->select('tb_establecimiento.name',
                'tb_establecimiento.direccion',
                'tb_establecimiento.foto',
                'tb_notas.estado',
                'tb_notas.id',
                'tb_notas.created_at',
                'tb_notas.tiempo_estimado',
                'tb_notas.negocio_codigo',
                'tb_notas.usuario_codigo')
                ->where('tb_notas.usuario_codigo','=', $id)
                ->orderBy('tb_notas.id','DESC')->get();
        } else {
            $notas = DB::table('tb_notas')
                ->join('tb_establecimiento','tb_notas.negocio_codigo','=','tb_establecimiento.id')
                ->join('users','tb_establecimiento.usuario_codigo','=','users.id')
                ->select('tb_establecimiento.name',
                    'tb_establecimiento.direccion',
                    'tb_establecimiento.foto',
                    'tb_notas.estado',
                    'tb_notas.id',
                    'tb_notas.created_at',
                    'tb_notas.tiempo_estimado',
                    'tb_notas.negocio_codigo',
                    'users.name',
                    'tb_notas.usuario_codigo')
                    ->where('tb_establecimiento.usuario_codigo','=', $id)
                    ->orderBy('tb_notas.id','DESC')->get();

        }


        

                    
                return response()->json([
                    'notas' => $notas
                ]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function confirmacionPedido($id) {
        $notas = NotasModel::query()->where('id', $id)->first();
        
        return response()->json([
            'notas' => $notas
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *php artisan krlove:generate:model User  --table-name=user
     * @param  \Illuminate\Http\Request  $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
       $request['created_at']      = $request['created_at'];
       $request['latitud']         = $request['latitud'];
       $request['longitud']        = $request['longitud'];
       $request['negocio_codigo']  = $request['negocio_codigo'];
       $request['tercero_codigo']  = $request['tercero_codigo'];
       $request['usuario_codigo']  = $request['usuario_codigo'];
       $request['valor']           = $request['valor'];
       $request['updated_at']      = $request['updated_at'];
       $request['titulo']          = $request['titulo'];
       $request['domicilio']       = 'SI';
       $request['descripcion']     = $request['descripcion'];
       $request['valor_domicilio'] = $request['valor_domicilio'];
       $request['estado'] = $request['estado'];

        

        $respuesta =  NotasModel::create($request->all());
    
        if($request->data) {
            foreach ($request->data as $key => $value) {

                $request['pedido']      = $value['name'];
                $request['nota_codigo'] = $respuesta['id'];
                $request['created_at']  = date("Y-m-d");
                $request['updated_at']  = date("Y-m-d");
                $respuestaDetalle =  DetalleNotasModel::create($request->all());
            }
            
            if($respuestaDetalle){
                $respuestaRegistro = "Pedido enviado, pronto se notificara el envio";
            }
        }

        return response()->json([
            'respuesta' => $respuestaRegistro,
            'data' => $respuesta
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    //  $notas = NotasModel::query()->where('id', $id)->first();
     // $negocio = EstablecimientoModel::query()->where('id', $notas->negocio_codigo)->get();
    

      $notas = DB::table('tb_notas')
      ->join('tb_establecimiento','tb_notas.negocio_codigo','=','tb_establecimiento.id')
      ->join('users','tb_notas.usuario_codigo','=','users.id')
      ->leftJoin('tb_tercero','tb_notas.tercero_codigo','=','tb_tercero.id')
      ->select('tb_establecimiento.name as establecimiento',
          'tb_establecimiento.direccion',
          'tb_establecimiento.foto',
          'tb_tercero.name as tercero',
          'tb_notas.estado',
          'tb_establecimiento.id as id_negocio',
          'tb_notas.id',
          'tb_notas.created_at',
          'tb_notas.tiempo_estimado',
          'tb_notas.negocio_codigo',
          'tb_notas.tiempo_estimado',
          'tb_notas.valor',
          'tb_notas.valor_domicilio',
          'users.name',
          'tb_notas.usuario_codigo')
          ->where('tb_notas.id','=', $id)->get();

          $detalle = DetalleNotasModel::where('nota_codigo', $id)->get();


      return response()->json([
        'notas' => $notas,
        'detalle' => $detalle
    ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $respuesta = "";
        $estado = null;
        $request['updated_at']  = date("Y-m-d");
        $update  =  NotasModel::find($id)->update($request->all());

        if($update) {
            $estado = 1;
            $respuesta = "Se le Notificara al Cliente el estado de su Pedido!";
        } else {
            $respuesta = "Validar Todos los Campos";
            $estado = 0;
        }
        return response()->json([
            'respuesta' =>  $respuesta,
            'estado' => $estado
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
