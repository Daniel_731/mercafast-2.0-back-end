<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Storage;
use DateTime;
use Illuminate\Support\Facades\DB;

class UsuarioController extends Controller
{
   /**
    * Display a listing of the resource.
    *
    * @param Request $request
    *
    * @return JsonResponse
    */
   public function index(Request $request)
   {
      $user = User::query()->where('email', $request->email)->first();

      if (!$user) {

         return response()->json([
            'message' => 0
         ]);

      } else {

         if ($user->tipo_login != $request->tipo_usuario_front) {

            $tipo_cuenta = "";

            switch ($user->tipo_login) {
               case 1:
                  $tipo_cuenta = 'con Google';
                  break;
               case 2:
                  $tipo_cuenta = 'con Facebook';
                  break;
               case 3:
                  $tipo_cuenta = 'de forma convencional';
                  break;

            }

            return response()->json([
               'message' => 'Debe iniciar sesi贸n ' . $tipo_cuenta,
               'user' => null,
               'token' => null
            ]);
         }

         if (!$user || !Hash::check($request->password, $user->password)) {

            return response()->json([
               'message' => 'Usuario 贸 password incorrectos',
               'user' => null,
               'token' => null
            ]);

         } else {

            $token = $user->createToken($request->device_name)->plainTextToken;

            return response()->json([
               'message' => $user->name . ' inicio sesi贸n',
               'user' => $user,
               'token' => $token
            ]);
         }

      }
   }


   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
      //
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {

      dd("ksbdkls");
      return response()->json([
         'usuario' => "Esta llegando al store"
      ]);
   }

   /**
    * Display the specified resource.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */

   public function show($id)
   {
      $usuario = User::where('id', $id)->get();
      return response()->json([
         'usuario' => $usuario
      ]);
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {
      //
   }

   /**
    * Update the specified resource in storage.
    *
    * @param \Illuminate\Http\Request $request
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, $id)
   {

      $imagen64 = $request->all()['url_imagen'];

      $domiciliario = "";

      $url = null;

      if ($imagen64) {

         $replace = substr($imagen64, 0, strpos($imagen64, ',') + 1);
         $image = str_replace($replace, '', $imagen64);
         $image = str_replace(' ', '+', $image);

         $nombre = new DateTime();
         $nombre = $nombre->getTimestamp() . ".png";
         Storage::disk('usuarios')->put($nombre, base64_decode($image));

         $url = $request->root() . '/storage/usuarios/' . $nombre;

         $request['url_imagen'] = $url;
      }

      $update = User::find($id)->update($request->all());

      $respuesta = "";
      if ($update) {
         $respuesta = "Su cuenta fue Actualizada!";
      } else {
         $respuesta = "No se pudo Actualizar";
      }
      return response()->json([
         "respuesta" => $respuesta
      ]);
   }

   public function cambioClave(Request $request)
   {
      $emailUser = $request->all()['email'];

      //$usuario = User::where('email', "$emailUser")->get();
      $usuario = DB::select("SELECT * FROM users WHERE email = '$emailUser'");

      try {

         if ($usuario) {
            $idUser = $usuario[0]->id;
            $email = $usuario[0]->email;
            $nombres = $usuario[0]->name;
            mt_srand(8);

            $contrasenia = mt_rand();
            $request['password'] = bcrypt($contrasenia);

            $update = User::find($idUser)->update($request->all());

            if ($update) {


               $envioEmail = new GeneralController();

               $envioEmail->correocambioClave($email, $nombres, $contrasenia);

               return response()->json([
                  "respuesta" => "La nueva Contraseña ha sido enviada a tú Correo Electronico",
                  "estado" => true
               ]);
            }

         } else {
            return response()->json([
               "respuesta" => "El Correo ingresado no esta Registrado en Mercafast!",
               "estado" => false
            ]);

         }

      } catch (ModelNotFoundException $e) {
         DB::rollBack();
         return response()->json([
            "respuesta" => "El Correo ingresado no esta Registrado en Mercafast!",
            "estado" => false
         ]);
      }


      //$usuario = User::where('email', $id)->get();

      //$update  =  User::find($id)->update($request->all());
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
      //
   }
}
