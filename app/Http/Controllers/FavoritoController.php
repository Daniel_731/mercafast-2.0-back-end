<?php

namespace App\Http\Controllers;

use App\FavoritoModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\Integer;

class FavoritoController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return void
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return void
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 *
	 * @return JsonResponse
	 */
	public function store(Request $request)
	{
		try {

			// Define rules validate
			$validator = Validator::make($request->all(), [
				'usuario_codigo' => ['required', 'integer'],
				'negocio_codigo' => ['required', 'integer']
			]);

			// Validate data before create record
			if ($validator->fails()) {
				return response()->json([
					'status' => false,
					'error' => $validator
				]);
			}


			// Create record
			$favorite = FavoritoModel::query()->create([
				'usuario_codigo' => $request['usuario_codigo'],
				'negocio_codigo' => $request['negocio_codigo']
			]);

			// Validate if record create
			if ($favorite) {
				return response()->json([
					'status' => true,
					'data'   => $favorite
				]);
			}


			// If not create record
			return response()->json([
				'status' => false,
				'error' => 'Ocurrio un problema al crear el registro'
			]);

		} catch (\Exception $e) {

			return response()->json([
				'status' => false,
				'error'  => $e->getMessage()
			]);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param FavoritoModel $favoritoModel
	 *
	 * @return JsonResponse
	 */
	public function show(FavoritoModel $favoritoModel)
	{
		try {

			// Validate basic data for delete record
			if ($favoritoModel && $favoritoModel->id) {

				// Find and delete record
				$favorite = FavoritoModel::query()->find($favoritoModel->id);

				// Validate record deleted
				if ($favorite) {
					return response()->json([
						'status' => true,
						'data'   => $favorite
					]);
				}
			}


			return response()->json([
				'status' => false
			]);

		} catch (\Exception $e) {

			return response()->json([
				'status' => false,
				'error' => $e->getMessage()
			]);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param FavoritoModel $favoritoModel
	 *
	 * @return void
	 */
	public function edit(FavoritoModel $favoritoModel)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @param FavoritoModel $favoritoModel
	 *
	 * @return void
	 */
	public function update(Request $request, FavoritoModel $favoritoModel)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 * @return JsonResponse
	 */
	public function destroy($id)
	{
		try {

			// Validate basic data for delete record
			if ($id) {

				// Find and delete record
				$favorite = FavoritoModel::query()->find($id)->delete();

				return response()->json([
					'status' => $favorite
				]);
			}


			return response()->json([
				'status' => false
			]);

		} catch (\Exception $e) {

			return response()->json([
				'status' => false,
				'error' => $e->getMessage()
			]);
		}
	}

	/**
	 * Get favorite by
	 *
	 * @param Request $request
	 *
	 * @return JsonResponse
	 */
	public function showByUserAndEstablishment(Request $request)
	{
		try {

			// Validate basic data for delete record
			if ($request && $request['usuario_codigo'] && $request['negocio_codigo']) {

				// Find and delete record
				$favorite = FavoritoModel::query()
					->where('usuario_codigo', '=', $request['usuario_codigo'])
					->where('negocio_codigo', '=', $request['negocio_codigo'])
					->get();

				// Validate record deleted
				if (count($favorite) > 0) {
					return response()->json([
						'status' => true,
						'data'   => $favorite
					]);
				}
			}


			return response()->json([
				'status' => false
			]);

		} catch (\Exception $e) {

			return response()->json([
				'status' => false,
				'error' => $e->getMessage()
			]);
		}
	}
}
