<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Mail;

class GeneralController extends Controller
{


   public function  registroUsuarios($email = null, $nombre = null, $clave = null) {

   }

   public function  pedidosSolicitados($email = null, $data = null) {

   }




   public function correocambioClave($email = null, $nombre = null, $clave = null)
   {



      try {

         ini_set('max_execution_time', 300);


         $template = [
            'name' => $nombre,
            'email' => $email,
            'clave' => $clave,
         ];


         Mail::send('plantillas.cambio-clave', $template, function ($message) use ($email) {
            $message->from('recuperacion@mercafast.com.co', 'Mercafast');
            $message->to($email, '');
            $message->subject('Bienvenidos a Mercafast!');
         });





         return response()->json([
            'success' => true
         ]);

      } catch (\Exception $e) {

         return response()->json([
            'status' => false,
            'error' => $e->getMessage()
         ]);
      }
   }
   public function create()
   {
      //
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
      //
   }

   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
      //
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {
      //
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, $id)
   {
      //
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
      //
   }
}
