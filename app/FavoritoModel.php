<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer $id
 * @property integer $usuario_codigo
 * @property integer $negocio_codigo
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property EstablecimientoModel $tbEstablecimiento
 */
class FavoritoModel extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'tb_favorito';

	/**
	 * The "type" of the auto-incrementing ID.
	 *
	 * @var string
	 */
	protected $keyType = 'integer';

	/**
	 * @var array
	 */
	protected $fillable = ['id', 'usuario_codigo', 'negocio_codigo'];

	/**
	 * @return BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo('App\User', 'usuario_codigo');
	}

	/**
	 * @return BelongsTo
	 */
	public function tbEstablecimiento()
	{
		return $this->belongsTo('App\EstablecimientoModel', 'negocio_codigo');
	}
}
