<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $categoria_codigo
 * @property string $name
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property TbCategorium $tbCategorium
 * @property TbProducto[] $tbProductos
 */
class TipoProductoModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tb_tipoproducto';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['categoria_codigo', 'name', 'descripcion', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tbCategorium()
    {
        return $this->belongsTo('App\TbCategorium', 'categoria_codigo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tbProductos()
    {
        return $this->hasMany('App\TbProducto', 'tipo_producto');
    }
}
