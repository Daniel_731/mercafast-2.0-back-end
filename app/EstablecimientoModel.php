<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property integer $id
 * @property string $name
 * @property string $descripcion
 * @property string $barrio
 * @property string $direccion
 * @property integer $catecoria_codigo
 * @property string $foto
 * @property string $created_at
 * @property string $updated_at
 * @property string $estado
 * @property integer $telefono
 * @property string $ubicacion
 * @property string $url
 * @property FavoritoModel[] $tbFavoritos
 */
class EstablecimientoModel extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'tb_establecimiento';

	/**
	 * The "type" of the auto-incrementing ID.
	 *
	 * @var string
	 */
	protected $keyType = 'integer';

	/**
	 * @var array
	 */
	protected $fillable = ['name',
		'descripcion',
		'barrio',
		'direccion',
		'catecoria_codigo',
		'foto',
		'created_at',
		'updated_at',
		'estado',
		'telefono',
		'ubicacion',
		'url',
		'hora_apertura',
		'hora_cierre',
		'ranking',
		'longitud',
		'latitud',
		'usuario_codigo',
		'tiempo_minimo',
		'tiempo_maximo'
	];

	/**
	 * @return HasMany
	 */
	public function tbFavoritos()
	{
		return $this->hasMany('App\FavoritoModel', 'negocio_codigo');
	}
}
