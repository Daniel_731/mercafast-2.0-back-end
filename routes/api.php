<?php

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
	return $request->user();
});


Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'UsuarioController@index');


Route::get('lista_categoria', 'CategoriaController@index');

/**
 * Domiciliarios o Terceros
 */
Route::post('crear_tercero', 'TerceroController@store');
Route::get('listar_domiciliarios', 'TerceroController@index');
Route::get('listado_unico_tercero/{data}', 'TerceroController@show');
Route::resource('actualizar_tercero', 'TerceroController');

/**
 * Establecimientos O negocios
 */
Route::post('crear_establecimiento', 'EstablecimientosController@store');
Route::get('listado_establecimiento', 'EstablecimientosController@index');
Route::get('listado_unico_establecimiento/{data}', 'EstablecimientosController@show');
Route::resource('actualizar_negocio', 'EstablecimientosController');

/**
 * Este encargara De traerLos negocios Para Clientes
 */
Route::get('establecimiento_principal/{data}', 'EstablecimientosController@edit');

/**
 * Muestra el detalle al que le realizara la nota
 */
Route::get('establecimiento_unico_principal/{data}', 'EstablecimientosController@establecimientoUnicoPrincipal');


Route::get('productos', 'ProductosController@index');


/*
 * Favorito
 */
Route::resource('favorito', 'FavoritoController');

Route::post('show_by_user_and_establishment', 'FavoritoController@showByUserAndEstablishment');


Route::post('/sanctum/token', function (Request $request) {

	$request->validate([
		'email' => 'required|email',
		'password' => 'required',
		'device_name' => 'required'
	]);

	$user = User::query()->where('email', $request->email)->first();

	if (!$user || !Hash::check($request->password, $user->password)) {
		throw ValidationException::withMessages([
			'email' => ['The provided credentials are incorrect.'],
		]);
	}

	$token = $user->createToken($request->device_name)->plainTextToken;

	$response = [
		'user' => $user,
		'token' => $token
	];

	return response($response, 201);

});


Route::post('/sanctum/remove_token', function (Request $request) {

	$user = User::query()->where('email', $request->email)->first();

	if ($user) {
		$user->tokens()->delete();
		Auth::logout();
	}

});

Route::get('listado_unica_cuenta/{id}', 'UsuarioController@show');
Route::resource('actualizar_mi_cuenta', 'UsuarioController');
Route::get('listar_favoritos_user/{id}', 'FavoritosController@index');
Route::post('borrar_negocio_favorito', 'FavoritosController@store');

/* Notas */

Route::post('registrar_pedidos','NotasController@store');
Route::resource('confirmar_pedidos', 'NotasController');
Route::get('listar_registro_pedidos/{id}', 'NotasController@index');
Route::get('mostrar_un_pedido/{id}', 'NotasController@show');

/**
 * listar terceros Unicos
 */
Route::get('tercero_unico_pedido/{id}', 'TerceroController@edit');
Route::get('listar_confirmacion_pedido/{id}', 'NotasController@confirmacionPedido');
Route::get('listar_confirmacion_tercero/{id}', 'TerceroController@TerceroConfirmado');

Route::resource('registrar_usuario_dispositivo','UsuarioDispositivoController');
Route::get('get_by_id_user/{id}', 'UsuarioDispositivoController@getByIdUser');

Route::post('registerUsers','UsuarioController@store');

Route::post('usuario/cambo-clave','UsuarioController@cambioClave');



